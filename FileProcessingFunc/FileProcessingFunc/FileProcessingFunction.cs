using System;
using System.Threading.Tasks;
using FileProcessingFunc.BusinessLogic;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Host;
using Microsoft.Extensions.Logging;

namespace FileProcessingFunc
{
    public class FileProcessingFunction
    {
        private readonly IBusinessModule _businessModul;
        public FileProcessingFunction(IBusinessModule businessModule)
        {
            _businessModul = businessModule;
        }
        [FunctionName("FileProcessingFunction")]
        public async Task Run([TimerTrigger("%TimerTrigger%")]TimerInfo myTimer, ILogger log)
        {
            log.LogInformation($"C# Timer trigger function executed at: {DateTime.Now}");
            //await _businessModul.ProcessFilesAsync();
            log.LogInformation($"C# Timer trigger function finished at: {DateTime.Now}");
        }
    }
}
