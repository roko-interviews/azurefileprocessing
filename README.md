# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
	Technical test for Cube. 
	Your task is to process a list of text files uploaded into an Azure file share* by users on daily basis**. 
	You'd be required to run a pattern*** for each text line of the file and if any matched then the file needs to be moved into a separate output folder or else it deleted from its original location
	You may submit your code to us via a GitHub link.
	-Azure file share opposed to a blob.
	-Assume there would be 1000's of files made available daily and the size could range from small to big ~500MB.
	-Pattern must be configuration and is a string that contain letters, numbers, ? and * symbols. ? stands for 1 any character, * stands for 0 or many of any characters. For instance, input 'abcd' matches pattern 'a*d' but input 'abcde' doesn't.
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
	Please make sure following configuration keys are present (with valid values) in the Application Settings of Azure function, 
	
    "StorageConnectionString": "",
    "StorageAccountName": "",
    "StorageAccountKey": "",
    "AzureFileShareName": "",
    "DirectoryForUplodedFiles": "",
    "DirectoryToCopyFiles": "",
    "Pattern": "",
    "TimerTrigger": "0 0 * * * *"

* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Brief description of the solution ###

* Solution
	The solution is a simple Azure function running on a schedule configured as Cron expression. Once executed the function processes files from Azure File Share service.
	The algorithm processes a file by checking the given pattern, if the pattern matches the file is moved to a different directory, otherwise deletes it from there.
	For searching the file content, internally Regex has been used. The input pattern is converted to equivalent Regex in such a way, so that it meets the pattern 
	symbols specification in requirement.

	Note: The problem statements/requirement provided were very brief, and open to asuumptions.
	List of assumptions:
	- Match should be any whole word in a given file. 
	- Partial match not soppurted, like, the pattern 'a*d' won't match 'abcde'.
	- Words might be separated by white-space, comma, semicolon, period.
	
* Limitations
	- No unit tests provided. The developer did some manual testing. If unit tests are mandatory for this techmical test, please inform me, I will provide some.
	- Exception handlings are general in nature and works by catching the base type. 
	- Implementation is not unit test friendly. Further design is needed to produce code that is 100% unit testable.

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact